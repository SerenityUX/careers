import {Component, OnInit, ViewChild} from '@angular/core';
import {CareersService} from "../careers.service";
import {debounceTime, distinctUntilChanged, finalize, switchMap, tap} from "rxjs/operators";
import {cities} from "../cities";
import {ConfigService} from "../config.service";
import {Observable} from "rxjs";
import {FormControl} from "@angular/forms";
import {MatTable} from "@angular/material/table";

@Component({
	selector: 'app-job-postings',
	templateUrl: './job-postings.component.html',
	styleUrls: ['./job-postings.component.scss']
})
export class JobPostingsComponent implements OnInit {
	public jobs = [];
	public loading: boolean;
	public noResults: boolean;
	public displayedColumns: string[] = ['title', 'category', 'location', 'created_at'];
	public cities = cities;
	public currentPage: number;
	public totalItems: number;
	public search: FormControl = new FormControl('');

	@ViewChild(MatTable) table: MatTable<any>;

	constructor(
		private careersService: CareersService,
		private configService: ConfigService,
	) {
	}

	ngOnInit(): void {
		this.configService.viewAsApplicant.asObservable().subscribe(res=> {
			if(res) {
				this.displayedColumns  = ['title', 'category', 'location', 'created_at'];
			} else {
				this.displayedColumns = ['title', 'category', 'location', 'created_at', 'view_applications'];
			}
		});

		this.configService.newJob.asObservable().subscribe(res => {
			if(res) {
				this.jobs.unshift(res);
				this.table.renderRows()
				this.totalItems =  this.totalItems + 1;
			}
		});

		this.getJobs(null).subscribe(res => {
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
			this.jobs = res.items;
		});

		this.search.valueChanges.pipe(
			debounceTime(300),
			distinctUntilChanged(),
			switchMap(()=> this.getJobs(null))
		).subscribe(res => {
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
			this.jobs = res.items;
		});
	}


	public getLocation(lat, lng): string {
		let location = this.cities.find(x => x.latitude == lat && x.longitude == lng);

		if (location) {
			return location.city + ', ' + location.state;
		} else {
			return '';
		}
	}

	public getJobs(page): Observable<any> {
		const search = {
			page: page,
			expression: []
		};

		const searchValue = this.search.value?.trim();

		if (searchValue) {
			search.expression.push({
				'type': 'group',
				'group': {
					'expression': [
						{
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'job.title'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'job.category'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						}
					]
				}
			});
		}

		return this.careersService.getJobs(search)
			.pipe(
				tap(() => this.loading = true),
				finalize(() => {
					this.noResults = !this.jobs.length
					this.loading = false
				})
			)
	}

	public pageChangeEvent(event): void {
		this.getJobs( event.pageIndex + 1).subscribe(res => {
			this.jobs = res?.items;
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
		});
	}

	public resetSearch() {
		this.search.reset();
		this.getJobs(null).subscribe(res=> {
			this.jobs = res?.items;
			this.table.renderRows();
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
		})
	}
}
