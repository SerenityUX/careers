import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter, map, mergeMap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';
import {PostJobPanelComponent} from "../post-job-panel/post-job-panel.component";

@Component({
	selector: 'app-action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {
	public isConfigured: boolean = false;
	public isHome: boolean = true;
	public config: XanoConfig;
	public user: any;
	public viewAsApplicant: boolean;

	constructor(
		private configService: ConfigService,
		private router: Router,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res => this.isConfigured = res);
		this.config = this.configService.config;

		this.configService.viewAsApplicant.asObservable().subscribe(res => {
			this.viewAsApplicant = !!res;
		})

		this.router.events.pipe(
			filter((event: RouterEvent) => event instanceof NavigationEnd),
			map(event => event.url)
		).subscribe(url => this.isHome = url === '/');

	}

	public showPanel(dialogType): void {
		let dialogRef;
		switch (dialogType) {
			case 'config':
				dialogRef = this.dialog.open(ConfigPanelComponent);
				break;
			case 'post-job':
				dialogRef = this.dialog.open(PostJobPanelComponent);
				dialogRef.afterClosed().subscribe(res=> {
					if(res?.new) {
						this.configService.newJob.next(res.item)
					}
				})
				break;
			default:
				break;
		}
	}

	public changeView() {
		this.configService.viewAsApplicant.next(!this.configService.viewAsApplicant.value)
	}

}
