import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class CareersService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public getCategories(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/category`,
		});
	}

	public getApplications(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/application`,
			params: {search}
		});
	}

	public getApplication(applicationID: number): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/application/${applicationID}`,
		});
	}

	public saveApplication(application: any): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/application`,
			params: application,
		});
	}

	public updateApplication(application): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/application/${application.id}`,
			params: application,
		});
	}

	public deleteApplication(applicationID: number): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/application/${applicationID}`,
		});
	}

	public getJobs(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/job`,
			params: {search}
		});
	}

	public getJob(jobId: string): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/job/${jobId}`,
		});
	}

	public saveJob(job): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/job`,
			params: job,
		});
	}

	public updateJob(job): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/job/${job.id}`,
			params: job,
		});
	}

	public deleteJob(jobID): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/job/${jobID}`,
		});
	}

	public upload(file): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/upload/attachment`,
			params: file
		});
	}
}
