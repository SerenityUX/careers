import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ViewJobComponent} from "./view-job/view-job.component";
import {ApplicationsComponent} from "./applications/applications.component";
import {EmployerGuard} from "./employer.guard";

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'applications', component: ApplicationsComponent, canActivate: [EmployerGuard]},
	{path: 'job/:id', component: ViewJobComponent},
	{path: 'job/:id/applications', component: ApplicationsComponent, canActivate: [EmployerGuard]},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
