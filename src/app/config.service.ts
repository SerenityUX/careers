import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null)
	public viewAsApplicant: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true)
	public newJob: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Xano Careers',
		summary: 'This is the demo of the Careers template that allows you to include a hiring page in your site.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/careers',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo uses a mock front-end to illustrate the use of the Xano Careers template. 
                	It enables you to post open positions, candidates to submit applications, and you to review 
                	those applications. Additionally, you can manage the status of submitted applications.
                </p>
                
                <p>
               		Authentication is omitted. However, it is recommended to enable authentication for the job table and viewing of submitted applications.
				</p>
				
				<p>
					This demo only serves as a demonstration of the capabilities of the template.
				</p>
				
				<h2>Views</h2>
				
				<p>
					This demo includes two different views: 
				</p>
				<ol>
					<li>
						The employer view: where you can post and edit job postings, view applications, 
						and manage the status of applications. 
					</li>
					<li>
						Applicant view: where you can view jobs and submit applications.
					</li>
				</ol>
	
				<p>
					Click <b>"View as..."</b> to toggle between both views. 
				</p>

                <h2>Components</h2>
              	<ul>
              		<li>
						<b>Job Postings</b>
						<p>
							A table view of all posted jobs. Additionally, you can search for job titles or job categories.
						</p>
					</li>
					<li>
						<b>View Job</b>
						<p>
							This component views the content of a job posting. If viewing as an employer, you can edit 
							the posting and navigate to applications, which shows filtered results. If viewing as an 
							applicant, you can apply for the job. 
						</p>
					</li>
					<li>
						<b>Job Management</b>
						<p>
							Where you can post new jobs and edit or delete existing ones.
						</p>
					</li>
					<li>
						<b>Apply for a Job</b>
						<p>
							This is the application panel, where applicants can submit their resume along with contact 
							information.
						</p>
					</li>
					<li>
						<b>Applications</b>
						<p>
							A table view of all submitted applications. Additionally, you can search by an applicant's name.
						</p>
					</li>
				</ul>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/job',
			'/job/{job_id}',
			'/application',
			'/application/{application_id}'
		]
	};

	constructor(private apiService: ApiService, private xanoService: XanoService) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
