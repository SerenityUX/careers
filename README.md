# Xano Careers Demo

This is a demo for the Careers template.

### Setup & Dependencies
This demo is intended to be as light as possible so outside of the core 
Angular packages only the following are included:

#### List of Dependencies
* material
* bootstrap css
* lodash-es
* ngx-quill
* quill

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`
